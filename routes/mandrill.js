const express = require('express');
const router = express.Router();
const mandrill = require('mandrill-api/mandrill');
const mandrill_client = new mandrill.Mandrill('4FE7ABKZNHoIDd6SyInzWA');
let message = {
  "html": "<p>Example HTML content: Mañana vemos como estructurar un proyecto node</p>",
  "text": "Example text content",
  "subject": "example subject",
  "from_email": "no-reply@airzonecontrol.com",
  "from_name": "Example Name",
  "to": [
    {
      "email": "mromeropolo@gmail.com",
      "name": "Recipient Name",
      "type": "to"
    },
    {
      "email": "mromero.arizone@gmail.com",
      "name": "Recipient2 Name",
      "type": "to"
    },
  ],
  "headers": {
    "Reply-To": "no-reply@airzonecontrol.com"
  },
  "important": false,
  "track_opens": null,
  "track_clicks": null,
  "auto_text": null,
  "auto_html": null,
  "inline_css": null,
  "url_strip_qs": null,
  "preserve_recipients": null,
  "view_content_link": null,
  "bcc_address": "mromeropolo@gmail.com",
  "tracking_domain": null,
  "signing_domain": null,
  "return_path_domain": null,
  "merge": true,
  "merge_language": "mailchimp",
  "global_merge_vars": [{
    "name": "merge1",
    "content": "merge1 content"
  }]
};
var async = false;
var ip_pool = "Main Pool";
var send_at = '2018-03-01 10:10:10';

router.get('/test', function(req, res, next) {
  mandrill_client.messages.send({"message": message, "async": async, "ip_pool": ip_pool, "send_at": send_at}, function(result) {
    console.log(result);
  }, function(e) {
    // Mandrill returns the error as an object with name and message keys
    console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
  });
  res.send('respond with a mail sent');
});

module.exports = router;
